package com.rd.pageindicatorview.slice;

import com.rd.PageIndicatorView;
import com.rd.pageindicatorview.ResourceTable;
import com.rd.pageindicatorview.data.Customization;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    PageSliderProvider provider;
    private Button btnCustomize;
    private PageSlider pageSlider;
    private PageIndicatorView pageIndicatorView;
    private Customization customization;
    private List<Component> lst;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        customization = new Customization();
        initViews();
    }

    private void initViews() {
        btnCustomize = (Button) findComponentById(ResourceTable.Id_btn_customize);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(Color.WHITE.getValue()));
        btnCustomize.setBackground(element);
        btnCustomize.requestFocus();

        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pl_main);
        pageIndicatorView = (PageIndicatorView) findComponentById(ResourceTable.Id_piv);
        pageIndicatorView.setPageSlider(pageSlider);
        pageIndicatorView.setSelectedColor(Color.getIntColor("#FF5252"));
        lst = createPageList();
        pageSlider.setProvider(
                new PageSliderProvider() {
                    @Override
                    public int getCount() {
                        return lst.size();
                    }

                    @Override
                    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                        Component component = lst.get(i);
                        componentContainer.addComponent(component);
                        return component;
                    }

                    @Override
                    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                        componentContainer.removeComponent((Component) o);
                    }

                    @Override
                    public boolean isPageMatchToObject(Component component, Object o) {
                        return component == o;
                    }
                });
        pageSlider.addPageChangedListener(
                new PageSlider.PageChangedListener() {
                    @Override
                    public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {}

                    @Override
                    public void onPageSlideStateChanged(int i) {}

                    @Override
                    public void onPageChosen(int i) {
                        pageIndicatorView.setSelection(i);
                    }
                });
        btnCustomize.setClickedListener(
                new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        Intent intent = new Intent();

                        intent.setParam(CustomizeAbilitySlice.EXTRAS_CUSTOMIZATION, customization);

                        Intent.OperationBuilder operationBuilder = new Intent.OperationBuilder();
                        operationBuilder.withAction("action.customize");
                        intent.setOperation(operationBuilder.build());
                        presentForResult(
                                new CustomizeAbilitySlice(),
                                intent,
                                CustomizeAbilitySlice.EXTRAS_CUSTOMIZATION_REQUEST_CODE);
                    }
                });
    }

    @Override
    protected void onResult(int requestCode, Intent resultIntent) {
        super.onResult(requestCode, resultIntent);

        if (customization != null && resultIntent != null) {
            this.customization = resultIntent.getSerializableParam(CustomizeAbilitySlice.EXTRAS_CUSTOMIZATION);

            updateIndicator();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private List<Component> createPageList() {
        List<Component> pageList = new ArrayList<>();
        pageList.add(createPageComponent(Color.BLUE.getValue()));
        pageList.add(createPageComponent(Color.GREEN.getValue()));
        pageList.add(createPageComponent(Color.RED.getValue()));
        pageList.add(createPageComponent(Color.YELLOW.getValue()));

        return pageList;
    }

    private Component createPageComponent(int color) {
        Component component = new Component(this);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(color));
        component.setBackground(element);
        return component;
    }

    private void updateIndicator() {
        if (customization == null) {
            return;
        }
        pageIndicatorView.setAnimationType(customization.getAnimationType());
        pageIndicatorView.setOrientation(customization.getOrientation());
        pageIndicatorView.setRtlMode(customization.getRtlMode());
        pageIndicatorView.setInteractiveAnimation(customization.isInteractiveAnimation());
        pageIndicatorView.setAutoVisibility(customization.isAutoVisibility());
        pageIndicatorView.setFadeOnIdle(customization.isFadeOnIdle());
    }
}
