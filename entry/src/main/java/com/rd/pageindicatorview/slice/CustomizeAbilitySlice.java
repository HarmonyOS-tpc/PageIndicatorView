package com.rd.pageindicatorview.slice;

import com.rd.pageindicatorview.ResourceTable;
import com.rd.pageindicatorview.data.Customization;
import com.rd.pageindicatorview.data.CustomizationConverter;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.dialog.ListDialog;

public class CustomizeAbilitySlice extends AbilitySlice
        implements Component.ClickedListener, AbsButton.CheckedStateChangedListener {
    public static final String EXTRAS_CUSTOMIZATION = "EXTRAS_CUSTOMIZATION";
    public static final int EXTRAS_CUSTOMIZATION_REQUEST_CODE = 1000;

    private static final String[] ORIETATION = {"Horizontal", "Vertical"};

    private static final String[] ANIMTION_TYPE = {
        "None", "Color", "Scale", "Worm", "Slide", "Fill", "Thin Worm", "Drop", "Swap", "Scale Down"
    };

    private static final String[] RTL = {"On", "Off", "Auto"};

    private Customization customization;

    private Button buttonAnimationType;
    private Button buttonOrietation;
    private Button buttonRtl;
    private Button buttonDone;
    private ListDialog dialogAnimationType;
    private ListDialog dialogOrientation;
    private ListDialog dialogRtl;
    private Switch switchAutoVisibility;
    private Switch switchFadeOnIdle;
    private Switch switchInteractiveAnimation;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_slice_customize);
        solveIntent(intent);
        initViews();
    }

    @Override
    public void terminateAbility() {
        super.terminateAbility();
    }

    private void solveIntent(Intent intent) {
        if (intent != null) {
            customization = intent.getSequenceableParam(EXTRAS_CUSTOMIZATION);
        } else {
            customization = new Customization();
        }
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
    }

    private void initViews() {
        buttonAnimationType = (Button) findComponentById(ResourceTable.Id_btn_animation_type);
        buttonOrietation = (Button) findComponentById(ResourceTable.Id_btn_orientation);
        buttonRtl = (Button) findComponentById(ResourceTable.Id_btn_rtl);
        buttonDone = (Button) findComponentById(ResourceTable.Id_btn_done);
        buttonOrietation.setClickedListener(this);
        buttonRtl.setClickedListener(this);
        buttonAnimationType.setClickedListener(this);
        buttonDone.setClickedListener(this);

        switchAutoVisibility = (Switch) findComponentById(ResourceTable.Id_sw_auto_visibility);
        switchFadeOnIdle = (Switch) findComponentById(ResourceTable.Id_sw_fa_on_idle);
        switchInteractiveAnimation = (Switch) findComponentById(ResourceTable.Id_sw_interactive_animation);
        switchAutoVisibility.setCheckedStateChangedListener(this);
        switchFadeOnIdle.setCheckedStateChangedListener(this);
        switchInteractiveAnimation.setCheckedStateChangedListener(this);

        initDialog();
    }

    private void initDialog() {
        dialogAnimationType = new ListDialog(this);
        dialogOrientation = new ListDialog(this);
        dialogRtl = new ListDialog(this);

        dialogAnimationType.setSingleSelectItems(ANIMTION_TYPE, 0);
        dialogAnimationType.setOnSingleSelectListener(
                new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int i) {
                        if (customization != null) {
                            customization.setAnimationType(CustomizationConverter.getAnimationType(i));
                            if (dialogAnimationType.isShowing()) {
                                dialogAnimationType.hide();
                            }
                        }
                    }
                });
        dialogOrientation.setSingleSelectItems(ORIETATION, 0);
        dialogOrientation.setOnSingleSelectListener(
                new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int i) {
                        if (customization != null) {
                            customization.setOrientation(CustomizationConverter.getOrientation(i));
                            if (dialogOrientation.isShowing()) {
                                dialogOrientation.hide();
                            }
                        }
                    }
                });
        dialogRtl.setSingleSelectItems(RTL, 0);
        dialogRtl.setOnSingleSelectListener(
                new IDialog.ClickedListener() {
                    @Override
                    public void onClick(IDialog iDialog, int i) {
                        if (customization != null) {
                            customization.setRtlMode(CustomizationConverter.getRtlMode(i));

                            buttonRtl.setText("set rtl " + RTL[i]);

                            if (dialogRtl.isShowing()) {
                                dialogRtl.hide();
                            }
                        }
                    }
                });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_animation_type:
                if (!dialogAnimationType.isShowing()) dialogAnimationType.show();
                break;
            case ResourceTable.Id_btn_orientation:
                if (!dialogOrientation.isShowing()) dialogOrientation.show();
                break;
            case ResourceTable.Id_btn_rtl:
                if (!dialogRtl.isShowing()) dialogRtl.show();
                break;
            case ResourceTable.Id_btn_done:
                Intent intent = new Intent();
                intent.setParam("EXTRAS_CUSTOMIZATION", customization);
                setResult(intent);
                terminate();
                break;
        }
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean isChecked) {
        switch (absButton.getId()) {
            case ResourceTable.Id_sw_auto_visibility:
                customization.setAutoVisibility(isChecked);
                break;
            case ResourceTable.Id_sw_interactive_animation:
                customization.setInteractiveAnimation(isChecked);
                break;
            case ResourceTable.Id_sw_fa_on_idle:
                customization.setFadeOnIdle(isChecked);
                break;
        }
    }
}
