

### **PageIndicatorView**

`PageIndicatorView` is light library to indicate PageSlider's selected page with different animations and ability to customise it as you need.

![](assets/preview_anim_drop.gif)


```groovy
implementation 'io.openharmony.tpc.thirdlib:pageindicatorview:1.0.1'
```


### **Usage Sample**
Usage of `PageIndicatorView` is quite simple. All you need to do is to declare a view in your `layout.xml`  and call `setSelection` method to select specific indicator - that's it!

```java
PageIndicatorView pageIndicatorView = (PageIndicatorView) findComponentById(ResourceTable.Id_piv);
        pageIndicatorView.setCount(5); // specify total count of indicators
        pageIndicatorView.setSelection(2);
```


But if you're as lazy as I'm - then there is another option to handle `PageIndicatorView`

```xml

<com.rd.PageIndicatorView
        ohos:id="$+id:piv"
        ohos:height="40vp"
        ohos:width="200vp"
        ohos:bottom_margin="48vp"
        ohos:background_element="#fffffff"
        ohos:layout_alignment="bottom|horizontal_center"
        ohos:piv_padding="12dp"
        ohos:piv_radius="8dp"/>

```
All the `piv_` attributes here are specific for `PageIndicatorView` so you can customise it as you want with attributes - pretty handy.

But what is more important here is  `ohos:piv_PageSlider="$+id:PageSlider"`.
What it actually do is catch up your `PageSlider` and automatically handles all the event's to selected the right page - so you don't need to call `setSelection` method on your own.

Another handy options here that works with your `PageSlider` as a whole is
`ohos:piv_dynamicCount="true"` and ` ohos:piv_interactiveAnimation="true"`

Dynamic count will automatically updates `PageIndicatorView` total count as you updates pages count in your `PageSlider` - so that's pretty useful.

While interactive animation will progress the animation process within your swipe position, which makes animation more natural and responsive to end user.


> ***Note***:  Because `setPageSliderId` uses an instance of `PageSlider`, using it in recycler could lead to id conflicts, so `PageIndicatorView` will not know properly what is the right `PageSlider` to work with. Instead you should handle selected indicators on your own programatically.


```java
    pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
             @Override
             public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
                 pageIndicatorView.setSelection(position);
             }
 
             @Override
             public void onPageSlideStateChanged(int i) {
 
             }
 
             @Override
             public void onPageChosen(int i) {
 
             }
         });
```


Here you can see all the animations `PageIndicatorView` support.

Name| Support version| Preview
-------- | --- | ---
`AnimationType.NONE`| 0.0.1 | ![anim_none](assets/anim_none.gif)
`AnimationType.COLOR`| 0.0.1 |![anim_color](assets/anim_color.gif)
`AnimationType.SCALE`| 0.0.1 |![anim_scale](assets/anim_scale.gif)
`AnimationType.SLIDE`| 0.0.1 |![anim_slide](assets/anim_slide.gif)
`AnimationType.WORM`| 0.0.1 |![anim_worm](assets/anim_worm.gif)
`AnimationType.FILL`| 0.0.6 |![anim_worm](assets/anim_fill.gif)
`AnimationType.THIN_WORM`| 0.0.7 |![anim_thin_worm](assets/anim_thin_worm.gif)
`AnimationType.DROP`| 0.1.0 |![anim_drop](assets/anim_drop.gif)
`AnimationType.SWAP`| 0.1.1 |![anim_swap](assets/anim_swap.gif)



```
 Copyright 2017 Roman Danylyk

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```