package com.rd.draw;

import com.rd.animation.data.Value;
import com.rd.draw.controller.AttributeController;
import com.rd.draw.controller.DrawController;
import com.rd.draw.controller.MeasureController;
import com.rd.draw.data.Indicator;

import ohos.agp.components.AttrSet;
import ohos.agp.render.Canvas;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.Pair;

public class DrawManager {
    private Indicator indicator;
    private DrawController drawController;
    private MeasureController measureController;
    private AttributeController attributeController;

    public DrawManager() {
        this.indicator = new Indicator();
        this.drawController = new DrawController(indicator);
        this.measureController = new MeasureController();
        this.attributeController = new AttributeController(indicator);
    }

    public Indicator indicator() {
        if (indicator == null) {
            indicator = new Indicator();
        }

        return indicator;
    }

    public void setClickListener(DrawController.ClickListener listener) {
        drawController.setClickListener(listener);
    }

    public void touch(TouchEvent event) {
        drawController.touch(event);
    }

    public void updateValue(Value value) {
        drawController.updateValue(value);
    }

    public void draw(Canvas canvas) {
        drawController.draw(canvas);
    }

    public Pair<Integer, Integer> measureViewSize(int widthMeasureSpec, int heightMeasureSpec) {
        return measureController.measureViewSize(indicator, widthMeasureSpec, heightMeasureSpec);
    }

    public void initAttributes(Context context, AttrSet attrs) {
        attributeController.init(context, attrs);
    }
}
