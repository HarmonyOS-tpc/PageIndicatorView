/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rd.utils;

import ohos.agp.colors.RgbColor;

public class AnimatorUtils {
    public static int getColor(float value, int colorStart, int colorEnd) {
        RgbColor start = RgbColor.fromArgbInt(colorStart);
        RgbColor end = RgbColor.fromArgbInt(colorEnd);
        int red = (int) (start.getRed() + (end.getRed() - start.getRed()) * value);
        int green = (int) (start.getGreen() + (end.getGreen() - start.getGreen()) * value);
        int blue = (int) (start.getBlue() + (end.getBlue() - start.getBlue()) * value);
        return new RgbColor(red, green, blue).asArgbInt();
    }

    public static int getIntValue(int fromValue, int toValue, float value) {
        return (int) (fromValue + (toValue - fromValue) * value);
    }

    public static float getInterpolation(float input) {
        return (float) (Math.cos((input + 1) * Math.PI) / 2.0f) + 0.5f;
    }
}
