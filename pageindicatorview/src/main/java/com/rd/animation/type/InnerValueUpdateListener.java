package com.rd.animation.type;

import ohos.agp.animation.AnimatorValue;

public interface InnerValueUpdateListener {
    void onUpdate(AnimatorValue animatorValue, float f);
}
