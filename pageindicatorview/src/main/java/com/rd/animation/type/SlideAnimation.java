package com.rd.animation.type;

import com.rd.animation.controller.ValueController;
import com.rd.animation.data.type.SlideAnimationValue;
import com.rd.utils.AnimatorUtils;

import ohos.agp.animation.AnimatorValue;

public class SlideAnimation extends BaseAnimation<ValueAnimator> {
    private static final String ANIMATION_COORDINATE = "ANIMATION_COORDINATE";
    private static final int COORDINATE_NONE = -1;

    private SlideAnimationValue value;
    private int coordinateStart = COORDINATE_NONE;
    private int coordinateEnd = COORDINATE_NONE;

    public SlideAnimation(ValueController.UpdateListener listener) {
        super(listener);
        value = new SlideAnimationValue();
    }

    @Override
    public ValueAnimator createAnimator() {
        ValueAnimator animator = new ValueAnimator();
        animator.setDuration(BaseAnimation.DEFAULT_ANIMATION_TIME);
        animator.setValueUpdateListener(
                new AnimatorValue.ValueUpdateListener() {
                    @Override
                    public void onUpdate(AnimatorValue animatorValue, float v) {
                        animator.onUpdate(animatorValue, v);
                        onAnimateUpdated(animatorValue);
                    }
                });

        return animator;
    }

    @Override
    public SlideAnimation progress(float progress) {
        if (animator != null) {
            long playTime = (long) (progress * animationDuration);
            animator.onUpdate(animator, AnimatorUtils.getInterpolation(progress));
            onAnimateUpdated(animator);
        }

        return this;
    }

    public SlideAnimation with(int coordinateStart, int coordinateEnd) {
        if (animator != null && hasChanges(coordinateStart, coordinateEnd)) {
            this.coordinateStart = coordinateStart;
            this.coordinateEnd = coordinateEnd;

            PropertyValuesHolder holder = createSlidePropertyHolder();
            animator.setInnerListener(
                    new InnerValueUpdateListener() {
                        @Override
                        public void onUpdate(AnimatorValue animatorValue, float f) {
                            coordinate = AnimatorUtils.getIntValue(holder.getStartValue(), holder.getEndValue(), f);
                        }
                    });
        }

        return this;
    }

    private PropertyValuesHolder createSlidePropertyHolder() {
        PropertyValuesHolder holder = PropertyValuesHolder.ofInt(coordinateStart, coordinateEnd);

        return holder;
    }

   private int coordinate;

    private void onAnimateUpdated(AnimatorValue animation) {
        value.setCoordinate(coordinate);

        if (listener != null) {
            listener.onValueUpdated(value);
        }
    }

    @SuppressWarnings("RedundantIfStatement")
    private boolean hasChanges(int coordinateStart, int coordinateEnd) {
        if (this.coordinateStart != coordinateStart) {
            return true;
        }

        if (this.coordinateEnd != coordinateEnd) {
            return true;
        }

        return false;
    }
}
