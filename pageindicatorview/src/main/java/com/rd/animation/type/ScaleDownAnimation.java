package com.rd.animation.type;

import com.rd.animation.controller.ValueController;

public class ScaleDownAnimation extends ScaleAnimation {
    public ScaleDownAnimation(ValueController.UpdateListener listener) {
        super(listener);
    }

    @Override
    protected PropertyValuesHolder createScalePropertyHolder(boolean isReverse) {
        String propertyName;
        int startRadiusValue;
        int endRadiusValue;

        if (isReverse) {
            propertyName = ANIMATION_SCALE_REVERSE;
            startRadiusValue = (int) (radius * scaleFactor);
            endRadiusValue = radius;
        } else {
            propertyName = ANIMATION_SCALE;
            startRadiusValue = radius;
            endRadiusValue = (int) (radius * scaleFactor);
        }

        PropertyValuesHolder holder = PropertyValuesHolder.ofInt(startRadiusValue, endRadiusValue);

        return holder;
    }
}
