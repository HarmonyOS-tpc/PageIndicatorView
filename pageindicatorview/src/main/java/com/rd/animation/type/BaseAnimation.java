package com.rd.animation.type;

import com.rd.animation.controller.ValueController;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;

public abstract class BaseAnimation<T extends Animator> {
    public static final int DEFAULT_ANIMATION_TIME = 350;
    protected long animationDuration = DEFAULT_ANIMATION_TIME;

    protected ValueController.UpdateListener listener;
    protected T animator;

    public BaseAnimation(ValueController.UpdateListener listener) {
        this.listener = listener;
        animator = createAnimator();
    }

    public ValueController.UpdateListener getListener() {
        return listener;
    }

    public abstract T createAnimator();

    public abstract BaseAnimation progress(float progress);

    public BaseAnimation duration(long duration) {
        animationDuration = duration;

        if (animator instanceof AnimatorValue) {
            ((AnimatorValue) animator).setDuration(animationDuration);
        }

        return this;
    }

    public void start() {
        if (animator != null && !animator.isRunning()) {
            animator.start();
        }
    }

    public void end() {
        if (animator != null && animator.isRunning()) {
            animator.end();
        }
    }
}
