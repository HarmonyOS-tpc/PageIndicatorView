/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.rd.animation.type;

import com.rd.animation.data.Value;

import ohos.agp.animation.AnimatorValue;

public class ValueAnimator extends AnimatorValue {

    protected InnerValueUpdateListener innerListener;

    private boolean isReverse;
    private Value value;


    public void setInnerListener(InnerValueUpdateListener innerListener) {
        this.innerListener = innerListener;
    }

    public void onUpdate(AnimatorValue animatorValue, float v) {
        if (innerListener != null) {
            innerListener.onUpdate(animatorValue, v);
        }
    }

    public boolean isReverse() {
        return isReverse;
    }

    public void setReverse(boolean reverse) {
        isReverse = reverse;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}
