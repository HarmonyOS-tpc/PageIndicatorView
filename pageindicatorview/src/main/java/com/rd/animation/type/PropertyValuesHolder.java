package com.rd.animation.type;

import ohos.agp.render.PixelMapHolder;

public class PropertyValuesHolder {
    int startValue;
    int endValue;

    private PropertyValuesHolder() {}

    public static PropertyValuesHolder ofInt(int startValue, int endValue) {
        PropertyValuesHolder holder = new PropertyValuesHolder();
        holder.setStartValue(startValue);
        holder.setEndValue(endValue);
        return holder;
    }

    public int getStartValue() {
        return startValue;
    }

    public void setStartValue(int startValue) {
        this.startValue = startValue;
    }

    public int getEndValue() {
        return endValue;
    }

    public void setEndValue(int endValue) {
        this.endValue = endValue;
    }
}
